package ru.sberbank.javaSchool.homework.task1;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {

    public static List<Integer> calcFibonacciNumSeq(int inputNum) throws Exception {
        List<Integer> result = new ArrayList<Integer>();
        result.add(1);

        if (inputNum < 1) {
            throw new IllegalArgumentException("Вводимое число не должно быть меньше единицы!");
        }

        for (int i = 0; i < inputNum - 1; i++) {
            if (i == 0) {
                result.add(result.get(i));
            } else {
                result.add(result.get(i - 1) + result.get(i));
            }
        }

        return result;
    }
}
