package ru.sberbank.javaSchool.homework.task1;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Scanner;

public class Main {
    static {
        System.setProperty("log4j.configuration", "file:src/main/resources/log4j.properties");
    }

    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws Exception {

        Scanner scanner = new Scanner(System.in);

        int inputNum = scanner.nextInt();
        long startTime = System.nanoTime();

        Fibonacci.calcFibonacciNumSeq(inputNum);

        long durationTimeNanoSec = System.nanoTime() - startTime;
        double durationTimeSec = (double) durationTimeNanoSec / 1000000000.0;

        logger.info("Длительность работы программы в секундах: " + durationTimeSec);
    }
}
