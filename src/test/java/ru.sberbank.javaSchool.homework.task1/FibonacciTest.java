package ru.sberbank.javaSchool.homework.task1;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class FibonacciTest {

    @Test
    public void calcFib_ONE_NUM() throws Exception {
        List<Integer> resultList = Fibonacci.calcFibonacciNumSeq(1);
        int result = resultList.get(0);

        assertEquals(result, 1);
    }

    @Test
    public void calcFib_PosNum() throws Exception {
        assertEquals(Fibonacci.calcFibonacciNumSeq(6).get(5).intValue(), 8);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getExceptionOnZERO() throws Exception {
        Fibonacci.calcFibonacciNumSeq(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getExceptionOnNegNum() throws Exception {
        Fibonacci.calcFibonacciNumSeq(-1);
    }

}